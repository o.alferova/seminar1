package org.game.player;

import java.util.List;

public class PlayersProvider {
    private final List<Player> players;
    private int positionCurrentGamer = 0;

    public PlayersProvider(List<Player> players) {
        this.players = players;
    }

    public Player getNext() {
        positionCurrentGamer = ++positionCurrentGamer % players.size();
        return players.get(positionCurrentGamer);
    }

    public Player getCurrent() {
        return players.get(positionCurrentGamer);
    }
}
